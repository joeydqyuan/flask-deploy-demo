import os
import logging


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'

    YUAN = 'yuan'

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    FLASK_DEBUG = 1
    DEBUG = True

    YUAN = os.environ.get('YUAN_DEV')


class TestingConfig(Config):
    TESTING = True


class ProductionConfig(Config):

    YUAN = os.environ.get('YUAN')

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)
        app.logger.setLevel(logging.INFO)


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig
}
