import multiprocessing


workers = multiprocessing.cpu_count() * 2 + 1

bind = 'unix:flask-web.sock'

umask = 0x7

loglevel = 'info'

accesslog = '/var/log/gunicorn/gunicorn_acess.log'

errorlog = '/var/log/gunicorn/gunicorn_error.log'


