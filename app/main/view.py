from flask import request, current_app, jsonify
from . import bp
import os


@bp.route('/hello')
def hello():
    current_app.logger.error('--------: ' + str(type(current_app.config['YUAN'])))
    current_app.logger.error('--------: ' + str(current_app.config['YUAN']))
    current_app.logger.error('--------: ' + os.getenv('FLASK_CONFIG'))

    current_app.logger.debug('this is debug logging.')
    current_app.logger.info('this is info logging.')
    current_app.logger.error('this is error logging.')
    return 'Hello World!'


@bp.route('/world/', methods=['POST'])
def world():
    req_data = request.get_json()
    current_app.logger.debug('this is flask logger {}'.format(req_data))
    current_app.logger.info('this is flask logger {}'.format(req_data))
    current_app.logger.error('this is flask logger {}'.format(req_data))
    return jsonify({'status': 'success'})
